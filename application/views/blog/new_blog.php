<!-- Page Content -->
<div class="container">

	<div class="row">
		<!-- Blog Post Content Column -->
		<div class="col-lg-8">
			<h1> Create new blog post</h1>

			<form role="form">
				<div class="form-group">
					<label for="title">Title of the blog</label>
					<input type="title" class="form-control" id="title" name="title">
				</div>
				<div class="form-group">
					<label for="title">Category of your post</label>
					<select class="form-control" name="category_id">
						<option value="wildlife">Wildlife</option>
						<option value="cars">Cars</option>
						<option value="cricket">Cricket</option>
						<option value="Citites">Cities</option>
						<option value="Computer">Computer Science</option>
					</select>
				</div>

				<div class="form-group">
					<label for="content">Your Blog Content</label>
					<textarea class="form-control" rows="8" name="content"></textarea>
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>
	<!-- /.row -->
</div>
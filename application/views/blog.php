<?php include_once 'Include/header.php';?>

<?php include_once 'Include/menu.php';?>
<div id="tooplate_main">
				<div class="col_fw_last">
					
					<div id="content">
						
						<div class="post_box">
							<h2>Gray Box Website Template</h2>
							<img src="images/tooplate_image_06.png"  alt="Image 06" />
							<p>Morbi venenatis augue sit amet ante facilisis feugiat sed in lectus. Validate <a href="http://validator.w3.org/check?uri=referer" rel="nofollow"><strong>XHTML</strong></a> and <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow"><strong>CSS</strong></a>. Vivamus imperdiet, ante a pretium vehicula, ante enim sodales mi, eu rutrum odio turpis eget arcu. Proin a elit nisl, id aliquam felis. Nunc ultrices iaculis quam, sed commodo erat tempus mollis. Duis ultricies nulla sed dolor egestas id vestibulum mi sollicitudin.</p>
								<a href="<?php echo base_url().'blog/posts'; ?>" class="more"></a>
							
							<div class="post_meta">
								<span class="cat">Posted in <a href="#">Website</a>, <a href="#">Design</a></span> | Date: June 24, 2048 | <a href="#">126 comments</a>							</div>
							
							<div class="cleaner"></div>
						</div>
						
						<div class="post_box">
							<h2>Fusce Placerat Ultrices Magna</h2>
							<img src="images/tooplate_image_07.png"  alt="Image 07" />
							<p>Mauris nisl mi, aliquet ac lobortis ut, tincidunt in tortor. Maecenas fermentum nisl vitae lectus dapibus pellentesque. Nunc viverra vestibulum magna, nec dignissim turpis rhoncus tincidunt. Donec ac nibh arcu. Fusce placerat ultrices magna, et blandit ipsum mattis eu. In justo augue, mollis sed suscipit non, laoreet et orci. Integer ac ultricies nisi.</p>
								<a href="<?php echo base_url().'../blog/posts'; ?>" class="more"></a>
							
							<div class="post_meta">
								<span class="cat">Posted in <a href="#">Illustrations</a>, <a href="#">Graphics</a></span> | Date: June 15, 2048 | <a href="#">140 comments</a>
							</div>
							
							<div class="cleaner"></div>
						</div>
						
						<div class="post_box">
							<h2>Nullam Volutpat Metus Eget</h2>
							<img src="images/tooplate_image_08.png"  alt="Image 08" />
							<p> Proin lobortis tellus ac tellus sollicitudin feugiat et at nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam volutpat, metus eget molestie scelerisque, nunc mi adipiscing justo, sit amet eleifend sapien urna ut nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
								<a href="<?php echo base_url().'../blog/posts'; ?>" class="more"></a>
							
							<div class="post_meta">
								<span class="cat">Posted in <a href="#">3D</a>, <a href="#">Animations</a></span> | Date: June 10, 2048 | <a href="#">158 comments</a>
							</div>
							
							<div class="cleaner"></div>
						</div>
						<div class="tooplate_paging">
							<ul>
								<li><a href="#" target="_parent">Previous</a></li>
								<li><a href="#" target="_parent">1</a></li>
								<li><a href="#" target="_parent">2</a></li>
								<li><a href="#" target="_parent">3</a></li>
								<li><a href="#" target="_parent">4</a></li>
								<li><a href="#" target="_parent">5</a></li>
								<li><a href="#" target="_parent">6</a></li>
								<li><a href="#" target="_parent">Next</a></li>
							</ul>
							
							<div class="cleaner"></div>
						</div>
					
					</div>
					
					<div id="sidebar">
						<h2>Categories</h2>
						
						<div class="sidebar_box">
							<ul class="sb_link">
								<li><a href="#">Aliquam erat volutpat</a></li>
								<li><a href="#">Cras ut ultricies sem</a></li>
								<li><a href="#">Maecenas vel lacus id</a></li>
								<li><a href="#">Ut porta lacus at mattis</a></li>
								<li><a href="#">Vehicula eget eleifend</a></li>
							</ul>
						</div>
						
						<h2>Our Services</h2>
						
						<div class="fp_service">
							
							<div class="fp_service_box fp_c1">
								<img src="images/20_64x64.png" alt="Image" />
								<p>
									<a href="#">Donec tempor nulla</a>
									Vivamus mollis, odio ut aliquam auctor, nisi purus placerat metus.
								</p>
								<div class="cleaner"></div>
							</div>
							
							<div class="fp_service_box fp_c2">
								<img src="images/38_64x64.png" alt="Image" />
								<p>
									<a href="#">Class aptent sociosqu</a>
									Curabitur leo elit, ultricies ac ultrices vitae, imperdiet id arcu.
								</p>
								<div class="cleaner"></div>
							</div>
							
							<div class="fp_service_box fp_c3">
								<img src="images/40_64x64.png" alt="Image" />
								<p>
									<a href="#">Fusce tempor magna</a>
									Suspendisse potenti, nulla eget velit ligula, a blandit est.
								</p>
								<div class="cleaner"></div>
							</div>
							
							<div class="fp_service_box fp_c4">
								<img src="images/46_64x64.png" alt="Image" />
								<p>
									<a href="#">Vestibulum nulla nisl</a>
									Ut aliquet sapien vel tortor dictum eu eleifend lorem blandit.
								</p>
								<div class="cleaner"></div>
							</div>
						
						</div>
						
						<div class="cleaner h20"></div>
							<a href="#" class="more float_r"></a>	
					
					</div>   
					
					<div class="cleaner"></div>
				</div>
				
			</div><!--end of tooplate_main-->
 <?php include_once 'Include/footer.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gray Box Theme - Free Website Template</title> 
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--
Template 2032 Gray Box
http://www.tooplate.com/view/2032-gray-box
-->
<link href="<?php echo base_url().'css/tooplate_style.css'?>" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">
	function clearText(field)
	{
		if (field.defaultValue == field.value) field.value = '';
		else if (field.value == '') field.value = field.defaultValue;
	}
    function validate(){
        if( document.contact.name.value == "")
        {
            alert( "Please provide your name!" );
            document.contact.name.focus() ;
            return false;
        }
        if( document.contact.email.value == "")
        {
            alert( "Please provide your Email!" );
            document.contact.email.focus() ;
            return false;
        }
        if( document.contact.password.value == "")
        {
            alert( "Please provide your Password!" );
            document.contact.password.focus() ;
            return false;
        }
        return( true );
    }
    function login_validate(){
        if( document.login.username.value == 'username')
        {
            alert( "Please provide your UserName!" );
            document.login.username.focus() ;
            return false;
        }
        if(document.login.password.value == 'password')
        {
            alert( "Please provide your Password!" );
            document.contact.password.focus() ;
            return false;
        }
        return( true );
    }

</script>
</script>

<script type="text/javascript" src="<?php echo base_url().'scripts/swfobject/swfobject.js'?>"></script>
        
<script type="text/javascript">
  	var flashvars = {};
    flashvars.cssSource = "css/piecemaker.css";
    flashvars.xmlSource = "piecemaker.xml";
		
    var params = {};
    params.play = "true";
    params.menu = "false";
    params.scale = "showall";
    params.wmode = "transparent";
    params.allowfullscreen = "true";
    params.allowscriptaccess = "always";
    params.allownetworking = "all";
	  
    swfobject.embedSWF('piecemaker.swf', 'piecemaker', '960', '500', '10', null, flashvars,    
    params, null);
    
</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/ddsmoothmenu.css'?>" />

<script type="text/javascript" src="<?php echo base_url().'scripts/jquery.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'scripts/ddsmoothmenu.js'?>">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

	ddsmoothmenu.init({
	mainmenuid: "tooplate_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
	})

</script>
	
</head>
<body>

<div id="tooplate_body_wrapper">
    <div id="tooplate_wrapper">
        <div id="tooplate_header">
            <div id="tooplate_top">
                <div id="tooplate_login">
                    <?php if($head == false): ?>
                        <form name="login" action="<?php echo base_url().'login'?>" method="post" onsubmit="return login_validate();">
                            <input type="text" value="username" name="u" size="10" id="username" title="username" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
                            <input type="password" value="password" name="p" size="10" id="password" title="password" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
                            <input type="submit" name="Search" value="" alt="Search" id="searchbutton" title="login" class="sub_btn" />
                        </form>
                    <?php else: ?>
                        <?php echo 'Welcome, '.ucfirst($auth[0]->username); ?>
                        <a href="<?php echo base_url() ?>">Logout</a>
                    <?php endif; ?>


                </div>
            </div>
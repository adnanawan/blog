<div id="tooplate_main">
	<div class="col_fw_last">

		<div class="col_allw300">
			<h2>Our Services</h2>

			<div class="fp_service">

				<div class="fp_service_box fp_c1">
					<img src="images/20_64x64.png" alt="Image" />
					<p>
						<a href="#">Donec tempor nulla</a>
						Vivamus mollis, odio ut aliquam auctor, nisi purus placerat metus.
					</p>

					<div class="cleaner"></div>
				</div>

				<div class="fp_service_box fp_c2">
					<img src="images/38_64x64.png" alt="Image" />
					<p>
						<a href="#">Class aptent sociosqu</a>
						Curabitur leo elit, ultricies ac ultrices vitae, imperdiet id arcu.
					</p>

					<div class="cleaner"></div>
				</div>

				<div class="fp_service_box fp_c3">
					<img src="images/40_64x64.png" alt="Image" />
					<p>
						<a href="#">Fusce tempor magna</a>
						Suspendisse potenti, nulla eget velit ligula, a blandit est.
					</p>

					<div class="cleaner"></div>
				</div>

				<div class="fp_service_box fp_c4">
					<img src="images/46_64x64.png" alt="Image" />
					<p>
						<a href="#">Vestibulum nulla nisl</a>
						Ut aliquet sapien vel tortor dictum eu eleifend lorem blandit.
					</p>

					<div class="cleaner"></div>
				</div>

			</div>

			<div class="cleaner h20"></div>
			<a href="#" class="more float_r"></a>

		</div>

		<div class="col_allw300">
			<h2>Quality Work</h2>
			<img src="images/tooplate_image_01.jpg" alt="Image 01" class="image_frame" />
			<div class="cleaner h20"></div>
			<p><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate scelerisque nulla eu porta.</em></p>
			<p>Gray Box is a free <a rel="nofollow" href="http://www.tooplate.com">website template</a> for everyone. You may edit and apply this template for any purpose. Credits go to <a rel="nofollow" href="http://www.photovaco.com" target="_blank">Free Photos</a> for photos and <a rel="nofollow" href="http://www.smashingmagazine.com" target="_blank">Smashing Magazine</a> for icons.</p>
			<div class="cleaner h20"></div>
			<a href="#" class="more float_r"></a>
		</div>

		<div class="col_allw300 col_rm">
			<h2>Our Profile</h2>
			<img src="images/tooplate_image_02.jpg" alt="Image 02" class="image_frame" />
			<div class="cleaner h20"></div>
			<p><em>Phasellus varius mi eu eros feugiat in eleifend massa imperdiet. Validate <a href="http://validator.w3.org/check?uri=referer" rel="nofollow"><strong>XHTML</strong></a> and <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow"><strong>CSS</strong></a>.</em></p>
			<ul class="tooplate_list">
				<li>Dictum eu pharetra quam semper</li>
				<li>Fusce fermentum justo non libero</li>
				<li>Vehicula eget eleifend eget</li>
				<li>Id pretium risus felis at libero</li>
				<li>Quisque eu elementum massa</li>
			</ul>

			<div class="cleaner h20"></div>
			<a href="#" class="more float_r"></a>
		</div>

		<div class="cleaner"></div>
	</div>
</div><!--end of tooplate_main-->
    
<div class="cleaner"></div>
		
		</div><!--end of tooplate_wrapper-->
</div><!--end of tooplate_body_wrapper-->
	
	<div id="tooplate_footer_wrapper">
    
		<div id="tooplate_footer">
		
			<div class="col_w240">
				<h5>Recent Posts</h5>
				<ul class="footer_link">
					<li><a href="#">Lorem ipsum dolor sit amet</a></li>
					<li><a href="#">Cras ut ultricies sem</a></li>
					<li><a href="#">Aliquam erat volutpat</a></li>
					<li><a href="#">Ut porta lacus at mattis</a></li>
					<li><a href="#">Maecenas vel lacus id lacus</a></li>
				</ul>
			</div>
			
			<div class="col_w240">
				<h5>Recent Comments</h5>
				<ul class="footer_link">
					<li><a href="#">James</a> on <a href="#">Mauris quam nibh</a></li>
					<li><a href="#">Johnson</a> on <a href="#">Curabitur imperdiet lacus</a></li>
					<li><a href="#">Nickey</a> on <a href="#">Donec tempor sagittis</a></li>
					<li><a href="#">Smith</a> on <a href="#">Vestibulum porttitor eleifend</a></li>
					<li><a href="#">George</a> on <a href="#">eNulla neque justo</a></li>
				</ul>
			</div>
			
			<div class="col_w240">
				<h5>Blogroll</h5>
				<ul class="footer_link">
					<li><a href="#">Class aptent taciti</a></li>
					<li><a href="#">Donec aliquam lorem</a></li>
					<li><a href="#">Phasellus rhoncus</a></li>
					<li><a href="#">Fusce tempor magna</a></li>
					<li><a href="#">Suspendisse et quam</a></li>
				</ul>
			</div>
			
			<div class="col_w240">
				<h5>Social Links</h5>
				<ul class="footer_link">
					<li><a href="#" class="facebook social">Facebook</a></li>
					<li><a href="#" class="linkedin social">Linkedin</a></li>
					<li><a href="#" class="myspace social">Myspace</a></li>
					<li><a href="#" class="youtube social">Youtube</a></li>
					<li><a href="#" class="vimeo social">Vimeo</a></li>
				</ul>
			</div>
			
			<div class="cleaner h40"></div>
				Copyright © 2048 <a href="#">Company Name</a>
			<div class="cleaner"></div>
		
		</div><!--end of tooplate_footer-->
	</div> <!--end of tooplate_footer_wrapper-->
  
</body>
</html>
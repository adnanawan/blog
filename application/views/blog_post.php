<?php include_once 'Include/header.php';?>

<?php include_once 'Include/menu.php';?>

<div id="tooplate_main">
				<div class="col_fw_last">
					
					<div id="content">
						
						<div class="post_box">
							<h2>Lorem ipsum dolor sit amet</h2>
							<img src="<?php echo base_url().'images/tooplate_image_06.png'?>"  alt="Image 06" />
							<p>Morbi venenatis augue sit amet ante facilisis feugiat sed in lectus. Vivamus imperdiet, ante a pretium vehicula, ante enim sodales mi, eu rutrum odio turpis eget arcu. Proin a elit nisl, id aliquam felis. Validate <a href="http://validator.w3.org/check?uri=referer" rel="nofollow"><strong>XHTML</strong></a> and <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow"><strong>CSS</strong></a>. Aenean vel vehicula augue. Vestibulum lectus sem, porttitor non molestie quis, pulvinar nec nulla. </p>
						  <p>Maecenas id orci vitae lectus fermentum posuere. <a href="#">Phasellus</a> lacinia eleifend elit, eu mollis erat consectetur et. Integer semper sollicitudin quam a ornare. Nam venenatis nibh ac sem faucibus et imperdiet magna laoreet. Sed at risus dui. Ut imperdiet libero at mauris vestibulum tempor. </p>
<p align="justify">Vestibulum at lorem ac lectus rhoncus aliquet eget ac mauris. Proin nec nunc magna, eu blandit massa. Sed elementum nisi ut quam vehicula eu egestas nisi varius. <a href="#">Aenean semper</a> convallis mi eu congue. In vel neque orci. Nunc vitae luctus ligula. Etiam vulputate semper lorem quis gravida. Donec nec aliquam ipsum.</p>
<div class="cleaner"></div>
						</div>
						
						<div class="cleaner h30"></div>
						<h3>Comments</h3>
								
						<div id="comment_section">
							<ol class="comments first_level">
								
								<li>
									<div class="comment_box commentbox1">
											
										<div class="gravatar">
											<img src="<?php echo base_url().'images/avator.jpg'?>" alt="Image" />
										</div>
										
										<div class="comment_text">
											<div class="comment_author">Steven <span class="date">May 26th, 2048</span> <span class="time">11:35 pm</span></div>
											<p>Vestibulum faucibus dolor eget sem imperdiet, a facilisis metus mollis. Nam a mauris dolor. Nulla eget tellus orci. Nullam placerat molestie diam congue consequat.</p>
										  <div class="reply"><a href="#">Reply</a></div>
										</div>
										<div class="cleaner"></div>
									</div>                        
									
								</li>
								
								<li>
								
								
										<ol class="comments">
									
											<li>
												<div class="comment_box commentbox2">
												
												<div class="gravatar">
												<img src="<?php echo base_url().'images/avator.jpg'?>" alt="Image" />
												</div>
												<div class="comment_text">
												<div class="comment_author">Ronald <span class="date">May 28th, 2048</span> <span class="time">10:24 pm</span></div>
												<p>Mauris bibendum mauris dolor, vitae ullamcorper nisi ullamcorper id. Fusce adipiscing dapibus velit, vel cursus nisi semper ac.</p>
												<div class="reply"><a href="#">Reply</a></div>
												</div>
												
												<div class="cleaner"></div>
												</div>                        
											
											
											</li>
											
											<li>
											
									
												<ol class="comments">
											
													<li>
														<div class="comment_box commentbox1">
														
															<div class="gravatar">
																<img src="<?php echo base_url().'images/avator.jpg'?>" alt="Image" />
															</div>
															
															<div class="comment_text">
																<div class="comment_author">Johnny <span class="date">June 15th, 2048</span> <span class="time">11:48 am</span></div>
																<p>Pellentesque eu tortor ac nunc vestibulum aliquam eget sit amet mi. In nec lorem metus. Nunc nec luctus tortor.</p>
															  <div class="reply"><a href="#">Reply</a></div>
															</div>
															
															<div class="cleaner"></div>
														</div>                        
													</li>
											
												</ol>

											</li>
										</ol>
										
								</li>
								
								<li>
									<div class="comment_box commentbox1">
											
											 
										<div class="gravatar">
											<img src="<?php echo base_url().'images/avator.jpg'?>" alt="Image" />
										</div>
										
										<div class="comment_text">
											<div class="comment_author">Van <span class="date">June 18th, 2048</span> <span class="time">07:35 am</span></div>
											<p>Sed adipiscing pulvinar risus, eu blandit nisl elementum a. Donec massa arcu, sodales et varius a, euismod id eros.</p>
										  <div class="reply"><a href="#">Reply</a></div>
										</div>
										
										<div class="cleaner"></div>
									</div>                        
								</li>
								
								<li>
									<div class="comment_box commentbox1">
											
										<div class="gravatar">
											<img src="<?php echo base_url().'images/avator.jpg'?>" alt="Image" />
										</div>
										
										<div class="comment_text">
											<div class="comment_author">David <span class="date">June 24th, 2048</span> <span class="time">10:54 am</span></div>
											<p>Phasellus erat mauris, molestie vel laoreet id, tempor ac nisl. Nulla id mauris ut sem sagittis dictum. Nullam laoreet nulla purus, quis consectetur elit varius at. </p>
										  <div class="reply"><a href="#">Reply</a></div>
										</div>
										
										<div class="cleaner"></div>
									</div>                        
								</li>
								
							</ol>
							
							<div class="cleaner h20"></div>    
							
							<div class="tooplate_paging">
								<ul>
									<li><a href="#" target="_parent">Previous</a></li>
									<li><a href="#" target="_parent">1</a></li>
									<li><a href="#" target="_parent">2</a></li>
									<li><a href="#" target="_parent">3</a></li>
									<li><a href="#" target="_parent">4</a></li>
									<li><a href="#" target="_parent">5</a></li>
									<li><a href="#" target="_parent">6</a></li>
									<li><a href="#" target="_parent">Next</a></li>
								</ul>
							
								<div class="cleaner"></div>
							
							</div>    
							
						</div>
						
						<div class="cleaner h20"></div>
						
						<div id="comment_form">
							<h3>Leave your comment</h3>
							
							<form action="#" method="get"> 
								<div class="form_row"> 
									<label>Name (* required )</label><br /> 
									<input type="text" name="name" /> 
								</div> 
								<div class="form_row"> 
									<label>Email  (* required)</label><br /> 
									<input type="text" name="name" /> 
								</div> 
								<div class="form_row"> 
									<label>Comment</label><br /> 
									<textarea  name="comment" rows="" cols=""></textarea> 
								</div> 
								<input type="submit" name="Submit" value="Submit" class="submit_btn" /> 
							</form> 
							
						</div>
					
					</div>
					
					<div id="sidebar">
						
						<h2>Categories</h2>
						<div class="sidebar_box">
							<ul class="sb_link">
								<li><a href="#">Vehicula eget eleifend eget</a></li>
								<li><a href="#">Cras ut ultricies sem</a></li>
								<li><a href="#">Aliquam erat volutpat</a></li>
								<li><a href="#">Ut porta, lacus at mattis</a></li>
								<li><a href="#">Maecenas vel lacus id lacus</a></li>
							</ul>
						</div>
						
						<h2>Our Services</h2>
						<div class="fp_service">
							
							<div class="fp_service_box fp_c1">
								<img src="<?php echo base_url().'images/20_64x64.png'?>" alt="Image" />
								<p>
									<a href="#">Donec tempor nulla</a>
									Vivamus mollis, odio ut aliquam auctor, nisi purus placerat metus.
								</p>
								<div class="cleaner"></div>
							</div>
							
							<div class="fp_service_box fp_c2">
								<img src="<?php echo base_url().'images/38_64x64.png'?>" alt="Image" />
								<p>
									<a href="#">Class aptent sociosqu</a>
									Curabitur leo elit, ultricies ac ultrices vitae, imperdiet id arcu.
								</p>
								<div class="cleaner"></div>
							</div>
							
							<div class="fp_service_box fp_c3">
								<img src="<?php echo base_url().'images/40_64x64.png'?>" alt="Image" />
								<p>
									<a href="#">Fusce tempor magna</a>
									Suspendisse potenti, nulla eget velit ligula, a blandit est.
								</p>
								<div class="cleaner"></div>
							</div>
							
							<div class="fp_service_box fp_c4">
								<img src="<?php echo base_url().'images/46_64x64.png'?>" alt="Image" />
								<p>
									<a href="#">Vestibulum nulla nisl</a>
									Ut aliquet sapien vel tortor dictum eu eleifend lorem blandit.
								</p>
								<div class="cleaner"></div>
							</div>
						</div>
						
						<div class="cleaner h20"></div>
						
						<a href="#" class="more float_r"></a>	
					</div>   
					
					<div class="cleaner"></div>
				
				</div>
				
			</div><!--end of tooplate_main-->
<?php include_once "Include/footer.php";?>
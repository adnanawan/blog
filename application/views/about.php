<?php include_once 'Include/header.php';?>

<?php include_once 'Include/menu.php';?>
<div id="tooplate_main">
	<div class="col_fw_last">

		<div class="col_w300 float_r">
			 <h2>Our Services</h2>
			<div class="fp_service">

				<div class="fp_service_box fp_c1">
					<img src="images/20_64x64.png" alt="Image" />
					<p>
						<a href="#">Donec tempor nulla</a>
						Vivamus mollis, odio ut aliquam auctor, nisi purus placerat metus.
					</p>
					<div class="cleaner"></div>
				</div>

				<div class="fp_service_box fp_c2">
					<img src="images/38_64x64.png" alt="Image" />
					<p>
						<a href="#">Class aptent sociosqu</a>
						Curabitur leo elit, ultricies ac ultrices vitae, imperdiet id arcu.
					</p>
					<div class="cleaner"></div>
				</div>

				<div class="fp_service_box fp_c3">
					<img src="images/40_64x64.png" alt="Image" />
					<p>
						<a href="#">Fusce tempor magna</a>
						Suspendisse potenti, nulla eget velit ligula, a blandit est.
					</p>
					<div class="cleaner"></div>
				</div>

				<div class="fp_service_box fp_c4">
					<img src="images/46_64x64.png" alt="Image" />
					<p>
						<a href="#">Vestibulum nulla nisl</a>
						Ut aliquet sapien vel tortor dictum eu eleifend lorem blandit.
					</p>
					<div class="cleaner"></div>
				</div>
			</div>

			<div class="cleaner h20"></div>

			<a href="#" class="more float_r"></a>

		</div>

		<div class="col_w630 float_l">
			<h2>About Our Company</h2>
			<img src="images/tooplate_image_01.jpg" alt="Image 01" class="image_fl" />
			<p><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</em></p>
			<p>Validate <a href="http://validator.w3.org/check?uri=referer" rel="nofollow"><strong>XHTML</strong></a> and <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow"><strong>CSS</strong></a>. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate scelerisque nulla eu porta. Phasellus vestibulum orci at eros commodo bibendum. Sed porttitor ligula nec nibh sagittis sit amet aliquet <a href="#">ante lobortis</a>. Phasellus luctus turpis eu justo volutpat euismod.</p>
			<div class="cleaner h40"></div>
			<img src="images/tooplate_image_02.jpg" alt="Image 02" class="image_fr" />
			<p><em>Aenean aliquet laoreet malesuada. Mauris at mi dui, eget faucibus sem.</em></p>
			<p> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices<a href="#"> posuere cubilia Curae</a>; Vestibulum nulla nisl, pulvinar sit amet fringilla ac, dignissim ac odio. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
			<div class="cleaner h20"></div>
			<a href="#" class="more float_r"></a>
		</div>

		<div class="cleaner"></div>
	</div>
</div><!--end of tooplate_main-->
<?php include_once 'Include/footer.php';?>
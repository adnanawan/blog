<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Adnan
 * Date: 04-Feb-16
 * Time: 2:44 AM
 */
class Blog extends CI_Controller
{
    public function __construct(){
        parent::__construct();
    }
	
	public function index()
    {
        $this->load->template('blog/index');
    }

    public function new_blog(){
        $this->load->template('blog/new_blog');
    }
}
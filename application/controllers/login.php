<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Adnan
 * Date: 06-Feb-16
 * Time: 2:58 AM
 */
class login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('blog_model');
    }

    function index()
    {
        $this->load->helper(array('form'));
        $q['auth'] = $this->user_model->login($this->input->post('u'), $this->input->post('p'));
        $q['head'] = true;
        $q['menu_name'] = $this->blog_model->getMainMenu();
        if($q['auth'] == true){

            $this->load->view('index', $q);
        }
        else{
            redirect('');
        }

    }

}

?>
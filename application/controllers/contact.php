<?php

/**
 * Created by PhpStorm.
 * User: Adnan
 * Date: 05-Feb-16
 * Time: 4:06 AM
 */
class Contact extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('blog_model');
    }
    public function index()
    {
        $menu['menu_name'] = $this->blog_model->getMainMenu();
        $this->load->view('contact', $menu);
    }
}
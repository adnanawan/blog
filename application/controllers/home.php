<?php

/**
 * Created by PhpStorm.
 * User: Adnan
 * Date: 05-Feb-16
 * Time: 3:57 AM
 */
class home extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('blog_model');
    }
    public function index()
    {
        $menu['menu_name'] = $this->blog_model->getMainMenu();
        $menu['head'] = false;
        $this->load->view('index', $menu);
    }
    public function categories(){
        $menu['menu_name'] = $this->blog_model->getMainMenu();
        $menu['head'] = false;
        $this->load->view('portfolio', $menu);
    }
}